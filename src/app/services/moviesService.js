function moviesService() {
  this.constant = {moviesShow: true,
    playerShow: true,
    moviesWatched: []
  };
}

moviesService.prototype = {
  checkMovieShow: function () {
    return this.constant.moviesShow;
  },

  checkPlayerShow: function () {
    return this.constant.playerShow;
  },
  showMovieShow: function () {
    this.constant.moviesShow = true;
  },
  cancelMovieShow: function () {
    this.constant.moviesShow = false;
  },
  showPlayerShow: function () {
    this.constant.PlayerShow = true;
  },
  cancelPlayerShow: function () {
    this.constant.PlayerShow = false;
  },
  addwatchedmovies: function (movie) {
    if (this.constant.moviesWatched.indexOf(movie) < 0) {
      this.constant.moviesWatched.push(movie);
    }
  },
  getWatchedMovies: function () {
    return this.constant.moviesWatched;
  }
};

module.exports = {
  moviesService: moviesService
};
