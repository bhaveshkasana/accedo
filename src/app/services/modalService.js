function modalService($uibModal, $log) {
  var $ctrl = this;

  $ctrl.animationsEnabled = true;

  $ctrl.open = function (size, movie) {
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: require('../components/modalTemplate.html'),
      controller: ModalInstanceCtrl,
      controllerAs: '$ctrl',
      size: size,
      resolve: {
        items: function () {
          return movie;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $ctrl.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $ctrl.toggleAnimation = function () {
    $ctrl.animationsEnabled = !$ctrl.animationsEnabled;
  };
}

var ModalInstanceCtrl = function ($uibModalInstance, items, $log, moviesService) {
  var $ctrl = this;
  $ctrl.movie = items;
  $ctrl.ok = function () {
    moviesService.addwatchedmovies($ctrl.movie);
  };
  $ctrl.cancel = function () {
    $log.info($ctrl.movie);
    $uibModalInstance.dismiss('cancel');
    $ctrl.movie = {};
  };
};

module.exports = {
  modalService: modalService
};
