function moviesResource($resource) {
  var ApiUrl = 'https://demo2697834.mockable.io/movies';
  return $resource(ApiUrl, {},
    {
      get: {}
    },
    {stripTrailingSlashes: false}
  );
}

module.exports = {
  moviesResource: moviesResource
};
