function watchedModalService($uibModal, $log) {
  var $ctrl = this;

  $ctrl.animationsEnabled = true;

  $ctrl.open = function (size, watched) {
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: require('../components/watchedModalTemplate.html'),
      controller: ModalInstanceCtrl,
      controllerAs: '$ctrl',
      size: size,
      resolve: {
        items: function () {
          return watched;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $ctrl.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $ctrl.toggleAnimation = function () {
    $ctrl.animationsEnabled = !$ctrl.animationsEnabled;
  };
}

var ModalInstanceCtrl = function ($uibModalInstance, items) {
  var $ctrl = this;
  $ctrl.watched = items;
  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
};

module.exports = {
  watchedModalService: watchedModalService
};
