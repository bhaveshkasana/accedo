module.exports = {
  template: require('./Header_comp.html'),
  controller: Header
};

function Header($scope, watchedModalService, moviesService) {
  this.name = "Bhavesh";
  $scope.name = this.name;
  this.showWatched = function () {
    var watched = moviesService.getWatchedMovies();
    watchedModalService.open('lg', watched);
  };
}
