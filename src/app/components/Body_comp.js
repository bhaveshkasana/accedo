module.exports = {
  template: require('./Body_comp.html'),
  controller: Body
};

function Body($scope, $log, moviesResource, moviesService) {
  this.show = moviesService.checkMovieShow();
  var promise;
  promise = moviesResource.get().$promise;
  promise.then(function (response) {
    $scope.movies = response.entries;
    $log.info($scope.movies);
  });
}
