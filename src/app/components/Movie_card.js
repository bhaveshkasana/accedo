module.exports = {
  template: require('./Movie_card.html'),
  controller: MovieCard,
  bindings: {
    movie: '<'
  }
};

function MovieCard($scope, $log, modalService) {
  this.show = true;
  $scope.handleClick = function (movie) {
    // $log.info(movie);
    modalService.open('lg', movie);
  };
}
