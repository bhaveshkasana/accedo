var angular = require('angular');
require('todomvc-app-css/index.css');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-resource');
require('angular-sanitize');
require('angular-animate');
var routesConfig = require('./routes');

var App = require('./app/containers/App');
var HeadComp = require('./app/components/Header_comp');
var BodyComp = require('./app/components/Body_comp');
var FootComp = require('./app/components/Footer_comp');
var moviesResource = require('./app/services/moviesResource');
var MovieCard = require('./app/components/Movie_card');
var moviesService = require('./app/services/moviesService');
var modalService = require('./app/services/modalService');
var watchedModalService = require('./app/services/watchedModalService');

import './index.css';

angular
  .module('app', ['ui.router', 'ngResource', 'ui.bootstrap', 'ngSanitize', 'ngAnimate'])
  .config(routesConfig)
  .service('moviesService', moviesService.moviesService)
  .service('modalService', modalService.modalService)
  .service('watchedModalService', watchedModalService.watchedModalService)
  .factory('moviesResource', moviesResource.moviesResource)
  .component('app', App)
  .component('headerComp', HeadComp)
  .component('bodyComp', BodyComp)
  .component('footComp', FootComp)
  .component('movieCard', MovieCard);
